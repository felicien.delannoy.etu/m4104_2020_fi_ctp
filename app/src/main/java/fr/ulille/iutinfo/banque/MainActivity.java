package fr.ulille.iutinfo.banque;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.icu.text.StringSearch;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public  static final String SOLDES_KEY = "Soldes";

    Spinner donneur;
    Spinner receveur;
    EditText montant;
    EditText operation;

    private String banque;
    private String joueurs[];
    private int soldeInitial;

    private int[] soldes;

    private class FlingDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            if ((Math.abs(velocityX) > Math.abs(velocityY)) && (velocityX < 0)) {

                // TODO Question 2.6 Lancement de la seconde activité

                // TODO Question 2.7 Communication entre activités

                return true;
            }
            return false;
        }
    }

    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO Question 2.1 Chargement des ressources
        this.banque = String.format(getString(R.string.banque));
        this.joueurs =getResources().getStringArray(R.array.nom_joueurs);
        this.soldeInitial= getResources().getInteger(R.integer.solde_initial);
        this.soldes= new int[4];
        this.soldes[0]=soldeInitial;
        this.soldes[1]=soldeInitial;
        this.soldes[2]=soldeInitial;
        this.soldes[3]=soldeInitial;

        // TODO Question 2.2 Affichage des noms des joueurs
        TextView viewJ1= (TextView)findViewById(R.id.joueur1);
        viewJ1.setText(this.joueurs[0]);
        TextView viewJ2=(TextView) findViewById(R.id.joueur2);
        viewJ2.setText(this.joueurs[1]);
        TextView viewJ3= (TextView)findViewById(R.id.joueur3);
        viewJ3.setText(this.joueurs[2]);
        TextView viewJ4= (TextView)findViewById(R.id.joueur4);
        viewJ4.setText(this.joueurs[3]);

        // TODO Question 2.5 Changement de configuration

        donneur = findViewById(R.id.donneur);
        receveur = findViewById(R.id.receveur);
        operation = findViewById(R.id.operation);
        montant = findViewById(R.id.montant);

        // TODO Question 2.3 Initialisation des spinners
        Spinner spinDonneur =(Spinner) findViewById(R.id.donneur);
        ArrayList<String> mesJoueurs = new ArrayList<String>();
        mesJoueurs.add(joueurs[0]);
        mesJoueurs.add(joueurs[1]);
        mesJoueurs.add(joueurs[2]);
        mesJoueurs.add(joueurs[3]);

        spinDonneur.setAdapter(new ArrayAdapter<String>( getBaseContext(),android.R.layout.simple_list_item_1,mesJoueurs));
        mDetector = new GestureDetectorCompat(this, new FlingDetector());

        update();
    }

    private void update() {
        // TODO Question 2.2 Affichage des soldes des joueurs
        TextView viewJ1 = (TextView)findViewById(R.id.solde1);
        viewJ1.setText(this.soldes[0]+"");
        TextView viewJ4 = (TextView)findViewById(R.id.solde4);
        viewJ4.setText(this.soldes[3]+"");
        TextView viewJ2 = (TextView)findViewById(R.id.solde2);
        viewJ2.setText(this.soldes[1]+"");
        TextView viewJ3 = (TextView)findViewById(R.id.solde3);
        viewJ3.setText(this.soldes[2]+"");

    }

    public void ajoute_operation(View view) {
        // TODO Question 2.4 ajout d'une transaction
        update();
    }

    // TODO Question 2.5 Changement de configuration

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }
}
